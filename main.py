#Author: Julian St John
#Date: None recently
import numpy as np, matplotlib.pyplot as plt, time
import imageAnalysis as iA, dictFileManager as dfm, imageReader as iR, cellFinder as cF

def runSingleAnalysis(targetDir: str,baseDir = dfm.currentDir(),expLength = 150):
    #creates file object
    fileIO = dfm.fileManager(targetDir,expLength,False)
    experiment = fileIO.getFileName(targetDir)

    outputName = experiment + "_results"
    outputPath = fileIO.makeNewPath(baseDir,outputName)
    fileIO.makeNewFolder(outputPath)

    for image in fileIO.fileData:
        if dfm.currentDir() != targetDir:
            fileIO.goToPath(targetDir)
        
        if iR.isTif(image) == False:
            continue
        
        arrayData = iR.getpixelDataToArray(image)
        fileIO.goToPath(outputPath)
        totalFluro = iA.generateData(image,arrayData)
        percInc = iA.getPercentIncrease(arrayData)
        iA.exportToCSV(experiment,image,totalFluro)

    print("Complete!")

def getBulkROIs(targetDir: str,expLength = 150):
    fileManager = dfm.fileManager(targetDir,expLength)
    listOfContainers = []

    for experiment in fileManager.fileData:
        print(f'Now analyzing {experiment}')
        #loads in filepaths from experiment in dictionary
        #in the form of a list of filePaths
        expPaths = fileManager.fileData.get(experiment)
        dirCount = len(expPaths)
        
        match dirCount:
            case 1:
                #print(f'{experiment} has only one folder location')
                workingDir = expPaths[0]
                fileManager.goToPath(workingDir)
            
                #creates a list of image NAMES
                
                images = list(filter(iR.isTif,fileManager.getFileNames(workingDir)))

                #fileManager.replaceSpaces(images)
                #uses the image names to then convert the image into data
                #which creates a list arrays that we can then pass onto the stats function
        
                for slide in images:
                    imagePath = fileManager.makeNewPath(workingDir,slide)
                    arrayData = list(iR.getpixelDataToArray(imagePath))
                    percInc = iA.getPercentIncrease(arrayData,50)
                    #creates a list containing objects of class "cell"
                    #see cellFinder.py
                    listOfCells = cF.searchMatrix(percInc)
                    cellStruct = cF.cellContainer(listOfCells)
                    
            case 0:
                print("Error: Files are not stored in the correct format. Ending Program.")
                print(experiment)
                break
            case other:
                #this shouldn't be an issue since you cannot have a negative amount of folders
                #BUT this is just in the off chance that there's a wacky error or bug produced
                if dirCount < 0:
                    print("Error: Invalid Number of Directories. Ending program")
                    break
                else:
                    for path in expPaths:
                        print(path)
                        workingDir = path
                        fileManager.goToPath(workingDir)

                        #creates a list of image NAMES
                        
                        images = list(filter(iR.isTif,fileManager.getFileNames(workingDir)))
                        #uses the image names to then convert the image into data
                        #which creates a list arrays that we can then pass onto the stats function
                
                        for slide in images:
                            imagePath = fileManager.makeNewPath(workingDir,slide)
                            arrayData = list(iR.getpixelDataToArray(imagePath))
                            percInc = iA.getPercentIncrease(arrayData,50)
                            cellList = cF.searchMatrix(percInc)
                            cellContainer = cF.cellContainer(cellList)
                            

    print("Complete!")

def getBulkTrainingData(targetDir: str,outputFileName: str,expLength = 150):
    fileManager = dfm.fileManager(targetDir,expLength)

    outputPath = fileManager.makeNewPath(fileManager.originalDirectory,outputFileName)
    fileManager.makeNewFolder(outputPath)

    for experiment in fileManager.fileData:
        print(f'Now analyzing {experiment}')
        #loads in filepaths from experiment in dictionary
        #in the form of a list of filePaths
        expPaths = fileManager.fileData.get(experiment)
        dirCount = len(expPaths)
        
        match dirCount:
            case 1:
                #print(f'{experiment} has only one folder location')
                workingDir = expPaths[0]
                fileManager.goToPath(workingDir)
            
                toAddPath = experiment + "_Results"

                outputFolderName = fileManager.makeNewPath(outputPath,toAddPath)
                fileManager.makeNewFolder(outputFolderName)

                #creates a list of image NAMES
                
                images = list(filter(iR.isTif,fileManager.getFileNames(workingDir)))

                #fileManager.replaceSpaces(images)
                #uses the image names to then convert the image into data
                #which creates a list arrays that we can then pass onto the stats function
        
                for slide in images:
                    imagePath = fileManager.makeNewPath(workingDir,slide)
                    fileManager.goToPath(outputFolderName)
                    arrayData = list(iR.getpixelDataToArray(imagePath))
                    percInc = iA.getPercentIncrease(arrayData,50)
                    fName = slide.split(".")[0]
                    iA.saveAsHeatmap(fName,percInc,fName +"percentIncrease")

            case 0:
                print("Error: Files are not stored in the correct format. Ending Program.")
                print(experiment)
                break
            case other:
                #this shouldn't be an issue since you cannot have a negative amount of folders
                #BUT this is just in the off chance that there's a wacky error or bug produced
                if dirCount < 0:
                    print("Error: Invalid Number of Directories. Ending program")
                    break
                else:
                    for path in expPaths:
                        print(path)
                        workingDir = path
                        fileManager.goToPath(workingDir)
                        toAddPath = experiment.replace(" ","_") + "_Results"

                        outputFolderName = fileManager.makeNewPath(outputPath,toAddPath)
                        fileManager.makeNewFolder(outputFolderName)

                        #creates a list of image NAMES
                        
                        images = list(filter(iR.isTif,fileManager.getFileNames(workingDir)))
                        #uses the image names to then convert the image into data
                        #which creates a list arrays that we can then pass onto the stats function
                
                        for slide in images:
                            imagePath = fileManager.makeNewPath(workingDir,slide)
                            fileManager.goToPath(outputFolderName)
                            arrayData = list(iR.getpixelDataToArray(imagePath))
                            percInc = iA.getPercentIncrease(arrayData,50)
                            fName = slide.split(".")[0]
                            iA.saveAsHeatmap(fName,percInc,fName +"percentIncrease")

    print("Complete!")

def runBulkAnalysis(targetDir: str,expLength = 150):
    #this line creates the object that holds all of the file data
    #you access the actual variable/data structure that holds the information
    #by using the content.fileData variable
    content = dfm.fileManager(targetDir,expLength)
    
    #this line below sets the output location. 
    outputPath = content.makeNewPath(content.originalDirectory,"Output")
    content.makeNewFolder(outputPath)

    for experiment in content.fileData:
        print(f'Now analyzing {experiment}')
        #loads in filepaths from experiment in dictionary
        #in the form of a list of filePaths
        expPaths = content.fileData.get(experiment)
        dirCount = len(expPaths)
        
        match dirCount:
            case 1:
                #print(f'{experiment} has only one folder location')
                workingDir = expPaths[0]
                content.goToPath(workingDir)
            
                toAddPath = experiment + "_Results"

                outputFolderName = content.makeNewPath(outputPath,toAddPath)
                content.makeNewFolder(outputFolderName)

                #creates a list of image NAMES
                
                images = list(filter(iR.isTif,content.getFileNames(workingDir)))

                #content.replaceSpaces(images)
                #uses the image names to then convert the image into data
                #which creates a list arrays that we can then pass onto the stats function
        
                for slide in images:
                    imagePath = content.makeNewPath(workingDir,slide)
                    content.goToPath(outputFolderName)
                    arrayData = list(iR.getpixelDataToArray(imagePath))
                    totalFluor = iA.generateData(slide,arrayData)
                    iA.exportToCSV(experiment,slide,totalFluor)  
            case 0:
                print("Error: Files are not stored in the correct format. Ending Program.")
                print(experiment)
                break
            case other:
                #this shouldn't be an issue since you cannot have a negative amount of folders
                #BUT this is just in the off chance that there's a wacky error or bug produced
                if dirCount < 0:
                    print("Error: Invalid Number of Directories. Ending program")
                    break
                else:
                    for path in expPaths:
                        print(path)
                        workingDir = path
                        content.goToPath(workingDir)
                        toAddPath = experiment.replace(" ","_") + "_Results"

                        outputFolderName = content.makeNewPath(outputPath,toAddPath)
                        content.makeNewFolder(outputFolderName)

                        #creates a list of image NAMES
                        
                        images = list(filter(iR.isTif,content.getFileNames(workingDir)))
                        #uses the image names to then convert the image into data
                        #which creates a list arrays that we can then pass onto the stats function
                
                        for slide in images:
                            imagePath = content.makeNewPath(workingDir,slide)
                            content.goToPath(outputFolderName)
                            arrayData = list(iR.getpixelDataToArray(imagePath))
                            totalFluor = iA.generateData(slide,arrayData)
                            iA.exportToCSV(experiment,slide,totalFluor)   

    print("Complete!")

def runScript():
    ioManager = dfm.IOManager()
    folders = ioManager.getFileNames()

    if "Images" not in folders:
        print("Images folder not found, creating image folders")
        status = ioManager.makeNewFolder("Images")

        match(status):
            case 0:
                print("Issue with folder creation. Ending Program")
                exit()
            case 1:
                print("Folder creation successful. Please place images that you would like to be analyzed within the folder")
                exit()
    else:
        stop = False
        while stop != True:
            if ioManager.getCurrentDir() != ioManager.startDir:
                ioManager.goToPath(ioManager.startDir)
            userInput = int(input("Please enter the number associated with the action you would like to do, then press enter afterwards:\n1) Single Experiment Analysis\n2) Bulk Experiment Analysis\n3) Cell ROI identification\n 4) ROI Analysis \n5) Exit\n"))

            match(userInput):
                case 1:
                    print("(Windows OS only) The filepath can be found using File Explorer, right clicking on the file, and selecting copy path")
                    targetFolder = str(input("Please input the desired filepath for the experiment, then press enter when complete: ")).strip('\"')
                    expTime = int(input("Please enter in the experiment length (in seconds): "))

                    if expTime != 150:
                        runSingleAnalysis(targetFolder,expLength= expTime)
                    else:
                        runSingleAnalysis(targetFolder)
                case 2:
                    print("Bulk Analysis Selected. All of the folders within the Images folder crated will now be analyzed")
                    runBulkAnalysis(ioManager.makeNewPath(ioManager.startDir,"Images"))
                case 3:
                    print("ROI generation selected. All experiments in the images folder will be used.")
                    getBulkROIs((ioManager.makeNewPath(ioManager.startDir,"Images")))
                case 4:
                    print("ROI Analysis Selected. This feature is still in development.")
                
                case 5:
                    print("Exiting Program")
                    stop = True
                case _: 
                    print("Input invalid, Please select a valid option")
            
#use this line below to set the value of the target directory
#to the path that holds all the images        
#targetDir = "C:\\Users\\jmagn\\Thesis_Code\\Images"
#runBulkAnalysis(targetDir)
#runSingleAnalysis("C:\\Users\\jmagn\\Thesis_Code\\Images\\220203_TNL_769_P6_Collagen")
#getBulkTrainingData(targetDir,"training_data_heatmaps")
#getBulkROIs(targetDir)

runScript()

