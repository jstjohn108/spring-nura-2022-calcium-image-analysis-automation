import imageReader as ir
import numpy as np
import matplotlib
import matplotlib.pyplot as plt 
#Hi it's past Julian, remember that at t = 50 (or t = 49 if you start at 0)
#that OT gets added, which is why there's a spike at fifty.

def generateData(fileName: str,imageData,expLength = 150)->list:
    fileName = fileName.split(".")[0].replace(" ","_")
    highFluoroIndex = getHighestFluroPhoto(imageData)
    maxFluoro = (imageData[highFluoroIndex],np.mean(imageData[highFluoroIndex]))
    florOverTime = getBulkTotalFlor(imageData)

    avgFluorOverTime = getBulkMeanFlor(imageData)
    #This list does the average but removes all elements that are less than 1
    absAvgFluoro = list(map(getAbsoluteAverageFlor,imageData))

    if expLength != len(avgFluorOverTime):
        axisLength = len(avgFluorOverTime)
    else:
        axisLength = expLength

    xAxisList = [ iter for iter in range(1, axisLength + 1)]

        # saves the heatmap of the total fluorescence
    totalHeatMapTitle = fileName + ": Total Instances of Fluorescence per Pixel"
    highestFluoroTitle = fileName +": Highest Fluorescence at t = " + str(highFluoroIndex + 1)  

    # saves heatmap of highest fluorescence
    arrayToLoad =  imageData[highFluoroIndex]
    nameToSaveAs = fileName + "_Slide_" + str(highFluoroIndex)

    summedFluoresence = getSummationHeatmap(imageData)
    #heatmaps of totalFluor and highestFluor
    saveAsHeatmap(
        fileName +"_TotalFluorescenceInstances", 
        summedFluoresence,
        totalHeatMapTitle)
    
    saveAsHeatmap(nameToSaveAs,arrayToLoad,highestFluoroTitle)
    # returns an array
    # average line graph
    saveLineGraph(fileName + "_AverageFluorescenceOverTime",xAxisList,avgFluorOverTime,fileName + ": Average Fluorescence Over Time","Time (s)","Number of Lit Pixels")

    # fluorsecence line graph
    saveLineGraph(fileName + "_TotalFluorescenceOverTime",xAxisList,florOverTime,fileName + ": Total Fluorescence Over Time","Time (s)","Number of Lit Pixels")

    
    return florOverTime

def getHighestFluroPhoto(nArrays: list)-> int:
    listOfSums = [0 for x in range (0,len(nArrays))]
    index = 0
    
    for x in nArrays:
        listOfSums[index] = np.sum(x)
        index = index + 1     

    return listOfSums.index(max(listOfSums))

def getTotalFlor(array)->int:
    return np.sum(array)

def getAverageFromList(input: list)->int:
    return sum(input)/len(input) 

def getAbsoluteAverageFlor(array)->float:
    return np.mean(buildAbsoluteArray(array))

def getAverageFlor(array)->float:
    return np.mean(array)

def buildAbsoluteArray(array)->np.array:
    array[array != 0] = 1
    return array

def getBulkMeanFlor(nArrayList: list):
    return list(map(getAverageFlor,nArrayList))

def getBulkTotalFlor(nArrayList: list):
    sumList = [n for n in range(0,len(nArrayList))]
    iter = 0
    for x in nArrayList:
        sumList[iter]= np.sum(x)
        iter = iter + 1
    
    return sumList

#We need to add some more functions to basically noise gate our images. 
'''
Functionally, I need to be able to utalize the getPercentIncrease 

'''
def getPercentIncrease(nArrayList: list,lowFrameIndex = None):
    '''
    This function takes two arrays (the lowest and the highest) and finds the percent difference 
    The returned value gives an array that compares the two. 
    Once the UI is implemented, I will most likely compare the percent increase with the first frame and 
    highest frame, then the 50th Frame and highest frame
    
    '''
    
    highestIndex = getHighestFluroPhoto(nArrayList)

    #this allows for comparisons at different times
    if lowFrameIndex == None:    
        startFrame = nArrayList[0]
    else:
        startFrame = nArrayList[lowFrameIndex]

    startFrame[startFrame == 0] = 1
        
    highFrame = nArrayList[highestIndex]

    diffArr = highFrame - startFrame
    percIncArr = (diffArr/startFrame)*100


    return percIncArr

#The below functions have to do with displaying the data. Since this file mainly has to do with
#Any changes to the numpy arrays and whatnot, it makes the most sense to group these functions 
#together with them
def displayAsHeatmap(array,title_name = "Placeholder"):
    plt.imshow(array, cmap = 'magma', interpolation = 'nearest')
    plt.title(title_name)
    plt.show()  
    plt.close()

def saveAsHeatmap(fileName: str,array,title_name = "Placeholder"):
    #print("Heatmap: attempting save")
    savedFileName = fileName + "_heatmap.png"
    plt.imshow(array, cmap = 'magma', interpolation = 'nearest')
    plt.title(title_name)
    plt.savefig(savedFileName)
    plt.close()

#this is a bad name for the funciton, because it returns a heatmap for the WHOLE experiment, not single images
def getSummationHeatmap(nArrays: list)-> np.array:
    sumArray = nArrays[0]
    firstIndex = False
    for x in nArrays:
        if firstIndex:
            sumArray = np.add(sumArray,x)
        else:
            firstIndex = True

    return sumArray

def displayLineGraph(xAxis: list, yAxis: list,name = "Default Graph", xLabel = "X-axis", yLabel = "Y-axis"):
    plt.plot(xAxis,yAxis)
    plt.title(name)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.show()
    plt.close()

def saveLineGraph(fileName: str,xAxis: list, yAxis: list,name = "Default Graph", xLabel = "X-axis", yLabel = "Y-axis"):
    #print("Line Graph: attempting save")
    saveName = fileName + "_lineGraph"
    plt.plot(xAxis,yAxis)
    plt.title(name)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.savefig(saveName)
    plt.close()

def displayBoxplot(toDisplay: list, fileName = "Placeholder",saveFig = False):
    if " " in fileName:
        saveName = fileName.replace(" ","")

    data = toDisplay
    fig = plt.figure(figsize=(10,7))

    plt.boxplot(data)
    plt.show()
    
    if saveFig == True:
        plt.savefig(saveName)
    
    plt.close()

def exportToCSV(expName: str, slideName: str,input: list, headers = []):
    #replaces spaces with underscores
    if " " in expName:
        expName = expName.replace(" ","_")
    
    #uses the passed experiment name to create the new filename
    fileName = expName + "_raw_data.txt"
    
    #headers is a variable that lets you add headers to the csv file
    #making for labeled data if that is something that you wanted to do
    toWrite = slideName + ","
    for number in input:
        toWrite = str(number) + ","
    
    if headers == []:
      with open(fileName, 'a') as file:
            file.write(toWrite)
              
def formatData(name: str, data: list)->str:
    lineOfText = name 
    for x in data:
        lineOfText = lineOfText + "," + str(x)

    return lineOfText    

