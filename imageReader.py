import os,matplotlib
import numpy as np
import matplotlib.pyplot as plt 
from skimage import io

def isStacked(array)->bool:
    if len(array.shape) == 3:
        return 1
    else:
        return 0

def isTif(fileName: str)->bool:
   if fileName.endswith(".tif"):
       return True
   else:
       return False

def getpixelDataToArray(fileName: str):
    imageArray = io.imread(fileName)
    if isStacked(imageArray):
        #print("Stacked image, Processing stack and returing pixel data")
        return getUnstackedTiff(fileName)   
    
    else:
        #print("Single Image, returning pixel data")
        return np.array(imageArray)

#this function may be depreciated 
def getbulkPixelDataToArray()->list:
    fileNames = os.listdir() 
    return list(map(getpixelDataToArray,fileNames))

def getUnstackedTiff(stackOfTiffs: str)->list:
    imageStack = io.imread(stackOfTiffs)
    arrList = [0 for iter in range(0,imageStack.shape[0])]

    for imageNumber in range(imageStack.shape[0]):
        arrList[imageNumber] = imageStack[imageNumber]
    
    return arrList

#All of this code below is depreciated and doesn't really need to be used.
def nameEditor(nameToBeChanged: str)->str:
    names = nameToBeChanged.split(".")
    fileName = names[0]; extension = names[1]
    number = int(fileName[-3:])
    number += 1
    number = str(number)

    match len(number):
        case 1:
            newName = fileName[:-3] + "0" + "0" + number + "." + extension
        case 2:
            newName = fileName[:-3] + "0" + number + "." + extension
        case 3:
            newName = fileName[:-3] + number + "." + extension
        case _:
            print("Error, should only be of length 1, 2, or 3. Ending Program")
            quit


    return newName

def getFileName()->str:   
    fileName = str(input("Please enter the name of the first file with the file extension included."))
    if fileName == "":
        file = "s_C001T001.tif"
    else:
        file = fileName
    
    return fileName







